# Autonomous Vehicles

## The Past, Present & Future of Autonomous Vehicles

## Introduction

Mechanical vehicles, in their own right, represent a wide array of impressive technological developments. Automated vehicular processes, and autonomous vehicles themselves, take that array even further, with technological advancements across multiple disciplines, while presenting challenges not only in the arena of tech, but also those of the social and the political. Much is to be gained, or lost, depending on whose perspective is taken.

## A History of Vehicular Autonomy and Automation

Automated vehicles have been present in the human imagination as early as 1478, as documented by Da Vinci's sketch of a pre-programmed clockwork cart, designed to move through a pre-determined course [[1](http://www.computerhistory.org/atchm/where-to-a-history-of-autonomous-vehicles/)]. Airplanes, trains, and sailboats were the first vehicles to incorporate a level of automation in their functioning. Trains did so by default, being given directional guidance from their tracks. Sailboats became self-steering with the addition of equipment that responds to wind direction [[2](https://en.wikipedia.org/wiki/Self-steering_gear)]. Airplanes automated stability, maintaining a given altitude and compass heading through adjustment of the hydraulic control system, in response to various inputs [[3](https://en.wikipedia.org/wiki/Gyroscopic_autopilot)].

Autonomous vehicles have been presented to us in science fiction as early as 1935, with David H. Kellar's _The Living Machine_, in which it is posited that:
> Thousands of needless deaths and injuries could be averted yearly if the operation of the automobile
> could be made absolutely safe without the slightest chance of accident except those which are intentional [[4](http://comicbookplus.com/?dlid=36256)].

One of the earliest autonomous concept vehicles -- which was, in essence, a life-sized remote-control car -- was presented to the public in 1926, by the Achen Motor Company [[5](https://news.google.com/newspapers?id=unBQAAAAIBAJ&sjid=QQ8EAAAAIBAJ&pg=7304%2C3766749)]. Another early concept vehicle, GM's Firebird II, would follow an electronic, road-embedded control strip, allowing for automatic steering [[6](http://www.oldcarbrochures.com/static/NA/GM%20Corporate%20and%20Concepts/1956_GM_Firebird_II/1956%20Firebird%20II-12-13.html)].

It must be noted that these vehicles, along with those that have followed, while improving greatly upon their precursors, all have only _automated processes_, and are not themselves _autonomous_. "Automated", here, refers to something, "... such as a factory or a system...", that is operated "... by using machines, computers, etc., instead of people to do the work" [[7](http://www.merriam-webster.com/dictionary/automated)]. "Autonomous", in contrast, refers to something that possesses or occupies "the quality or state of being self-governing", and has the freedom to self-direct [[8](http://www.merriam-webster.com/dictionary/autonomy)].

An autonomous vehicle, then, while being (in part) composed of automated processes, must be able to also control those processes, and decide when and how to engage them. Much as a human driver initiates processes in accordance with decisions or conclusions reached by combining the memory of experience with the interpretation of sensory input, an autonomous vehicle (in order to be called as such) must have a level of intelligence that renders it capable of complicated decision making. An autonomous vehicle needs to not only be aware of its environment, but also to be able to choose a correct course of action in that environment.

A number of corporations -- among which are [Tesla](https://www.teslamotors.com/en_CA/), [Audi](http://www.audi.ca/ca/web/en.html), [Volvo](http://www.volvocars.com/en-ca), and [Google](https://www.google.com/about/) -- are, at present, exploring both automated and autonomous vehicle technology.

## Companies Active in Autonomous Vehicle Technology

In October of 2014, Tesla began to equip their "Model S with hardware to allow for the incremental introduction of self-driving technology: a forward radar, a forward-looking camera, 12 long-range ultrasonic sensors positioned to sense 16 feet around the car in every direction at all speeds, and a high-precision digitally-controlled electric assist braking system". October of 2015 saw the release of version 7.0 of Tesla's software, allowing for improved coordination of the automated driving capabilities. This technology enables the following autopilot features: steering within lanes, changing lanes (with the tap of the turn signal), speed management with traffic-aware cruise control, parking space location, and automatic parallel parking. Furthermore, "[d]igital control of motors, brakes, and steering helps avoid collisions from the front and sides, as well as preventing the car from wandering off the road". It must be noted that, "[t]he driver is still responsible for, and ultimately in control of, the car"[[9](https://www.teslamotors.com/en_CA/blog/your-autopilot-has-arrived)]. 

As of January, 2015, Audi's prototype A7 provides autopilot only under the following specific conditions: exclusively on the freeway, during the day, with clear weather, a present and alert driver, and autopilot can only be engaged after entering the freeway, and must be disengaged before exiting the freeway [[10](http://fortune.com/2015/01/28/audis-self-driving-car/)].

Volvo's S90 has automated a range of safety features -- the vehicle detects pedestrians, cyclists, and other vehicles, and will apply the brakes as necessary to reduce the risk of collisions. The S90 also assists with steering, helping to keep the vehicle within lane markings, and can maintain a set speed or distance relative to the car ahead of it [[11](http://www.volvocars.com/en-ca/cars/new-models/s90)].

The autopilot features offered by Tesla, Audi, and Volvo represent an incredible array of capabilities -- however, they still 'only' amount to automated processes, not autonomous vehicles.

Google, on the other hand, has set its sights strictly on achieving a truly autonomous vehicle, that relies solely on its sensors and software for operation. Human piloting will be, in fact, not only unnecessary, but also impossible. As Google explains:
> We’re working toward vehicles that take you where you want to go at the push of a button. We started by adding components to existing cars like our Lexus SUVs, then began designing a new prototype from the ground up to better explore what should go into a fully self-driving vehicle. We removed the steering wheel and pedals, and instead designed a prototype that lets the software and sensors handle the driving.

This new type of vehicle, designed exclusively for self-driving, features an interior that is designed for riders (as opposed to drivers), a rounded shape that "maximizes sensor field of view", electric batteries that supply its power, a myriad of sensors to detect its surroundings, a computer to pilot the vehicle, and a set of backup systems "for steering, braking, computing and more" [[12](https://www.google.com/selfdrivingcar/how/)].

Google's Self-Driving Car Project has been in operation since 2009, and since its inception has driven over 1,000,000,000 miles autonomously, at first using a modified Toyota Prius, then a fleet of Lexus Rx540h's, and now, with its driverless prototype vehicle [[13](https://www.google.com/selfdrivingcar/where/)].

Google imagines the result of this project will be an increase in both safety and quality of life. According to the company's own promotional website:
> Imagine if everyone could get around easily and safely, regardless of their ability to drive.
>
>Aging or visually impaired loved ones wouldn't have to give up their independence. Time spent commuting could be time spent doing what you want to do. Deaths from traffic accidents—over 1.2 million worldwide every year—could be reduced dramatically, especially since 94% of accidents in the U.S. involve human error [[14](https://www.google.com/selfdrivingcar/)].

## Autonomous Technology

The technology that Google is using to create their driverless car is comprised of the following: topographic LIDAR, a camera on the front of the vehicle, bumper-mounted radar, an aerial for GPS, ultrasonic sensors on the rear wheels, altimeters, gyroscopes, and tachometers inside the vehicle, and a CPU to process the input from all of the aforementioned sensors. 

LIDAR, short for Light Detection and Ranging, "is a remote sensing method that uses light in the form of a pulsed laser to measure ranges (variable distances) to the Earth. These light pulses... generate precise, three-dimensional information about the shape of the Earth and its surface characteristics" -- so reads the definition provided by the American National Oceanic and Atmospheric Association.  Obviously, the NOAA uses LIDAR for massive, long-range surveying, but the technology can be applied on a much smaller scale, at street-level. [[15](http://oceanservice.noaa.gov/facts/lidar.html)].

The input from the vehicle's LIDAR is compared against data-rich maps which have been created in advance of the vehicle's operation. These maps are "... best thought of as ultra-precise digitizations of the physical world, all the way down to tiny details like the position and height of every single curb. A normal digital map would show a road intersection; these maps would have a precision measured in inches". This allows the vehicle to know precisely what the world around it looks like when it is empty, enabling the vehicle to swiftly identify the differences, and thus, to identify elements it must be aware of, and respond to [[16](http://www.theatlantic.com/technology/archive/2014/05/all-the-world-a-track-the-trick-that-makes-googles-self-driving-cars-work/370871/)].

Ford is working with the University of Michigan to produce vehicles able to better sense, and understand, the world around them under winter's snowy conditions, with 3D maps. Their maps are created under clear conditions, capturing both the road and above-ground features, "[t]hen, when snow covers the road, the car uses these above-ground markers to pinpoint its exact location and stay on the right path" [[17](http://blog.ford.ca/2016/01/06/ford-doubles-down-on-autonomous-vehicles/)].

The front-facing camera in Google's self-driving car (in combination with the vehicle's software) detects and tracks hazards -- from cones blocking off construction areas, to other motorists, cyclists, and pedestrians -- and reads road signs and traffic lights.

Radar, mounted on the bumpers, "... which is already used in intelligent cruise control, keeps track of other vehicles in front of and behind the car" [[18](http://www.alphr.com/cars/7038/how-do-googles-self-driving-cars-work)]. Radar, an acronym for "RAdio Detecting And Ranging", enables the vehicle to detect objects, determining both their position and velocity. Radar originally employed only the radio portion of the electromagnetic spectrum, which returned low resolution images (a result of the relatively large size of the wavelengths -- 10 to 13 m). Modern radar systems employ "the microwave and very high infrared portions of the electromagnetic spectrum", with wavelengths "from as small as 1mm up to 1m", enabling much higher resolutions [[19](https://www.iop.org/publications/iop/2011/file_47456.pdf)].

An aerial on the rear of the vehicle enables access to the Global Positioning System (GPS). According to the [Institute of Physics](http://www.iop.org/), the GPS "is a network of about 30 satellites orbiting the Earth at an altitude of 20,000 km" -- at least four of these satellites are accessible from any point on the planet at any given time. Using radio signals, each satellite "transmits information about its position and the current time at regular intervals". These signals are intercepted by a receiver, "which calculates how far away each satellite is based on how long it took for the messages to arrive" [[20](http://www.physics.org/article-questions.asp?id=55)]. Upon receiving signals from at least three satellites, the GPS receiver calculates its own position using trilateration. Trilateration works similarly to triangulation, in which a position on a map is determined through knowing the unknown position's distance relative to three other known positions -- if one makes circles around each known position, with radii equaling the distance between said known position and the position to be determined, the point at which the circles intercept one another is the desired position. Trilateration is the three-dimensional version of triangulation, where spheres are used instead of circles. If a signal can be received by a fourth satellite, then the calculations can be double-checked [[21](http://www.mio.com/technology-trilateration.htm)].

Ultrasonic sensors (on the rear wheels) "... keep track of the movements of the car and will alert the car about the obstacles in the rear... Cars that offer automatic ‘Reverse Park Assist’ technology utilise such sensors to help navigate the car into tight reverse parking spots. Typically, these sensors get activated when the car is engaged in the reverse gear" [[22](http://www.national.co.uk/tech-powers-google-car/)]. Ultrasonic sensors work through emitting a pulse of sound that is reflected off of nearby objects -- this reflection, or echo, is received by the sensor. According to Rockwell Automation, an industrial automation and information company:
>Ultrasonic sensing technology is based on the principle that sound has a relatively constant velocity. The time for an ultrasonic sensor’s beam to strike the target and return is directly proportional to the distance to the object. Consequently, ultrasonic sensors are used frequently for distance measurement applications such as level control.

They are also of great use in helping to determine the proximity of other vehicles.

>Ultrasonic sensors are capable of detecting most objects — metal or nonmetal, clear or opaque, liquid, solid, or granular — that have sufficient acoustic reflectivity. Another advantage of ultrasonic sensors is that they are less affected by condensing moisture than photoelectric sensors.

It must also be noted that "[a] downside to ultrasonic sensors is that sound absorbing materials, such as cloth, soft rubber, flour and foam, make poor target objects", reinforcing the importance of the many different types of sensors used by Google's self-driving car -- a single type of sensor alone would be insufficient for gathering enough input to create a digital version of the environment in which the vehicle finds itself [[23](http://www.ab.com/en/epub/catalogs/12772/6543185/12041221/12041229/print.html)].

The driverless car is also equipped with altimeters, gyroscopes, and tachometers. Altimeters determine altitude through measuring air pressure [[24](http://education.nationalgeographic.org/encyclopedia/altimeter/)]. According to Oxford Dictionaries, a gyroscope is "[a] device consisting of a wheel or disc mounted so that it can spin rapidly about an axis which is itself free to alter in direction. The orientation of the axis is not affected by tilting of the mounting, so gyroscopes can be used to provide stability or maintain a reference direction in navigation systems, automatic pilots, and stabilizers" [[25](http://www.oxforddictionaries.com/definition/english/gyroscope)]. A tachometer "... indicates the speed, usually in revolutions per minute, at which an engine shaft is rotating" [[26](http://www.encyclopedia.com/topic/tachometer.aspx)].

The computer in Google's self-driving car takes all of the data gathered by the sensors and turns it into information on which it can act. It does this in the context of all that has been gathered, processed, and learned by _all of the vehicles_ thus far. This means that logic is not hardcoded, but learned from real-world behaviour of other actors (pedestrians, cyclists, motorists, or even debris) in all of the environments that each vehicle has travelled through, enabling each vehicle to be flexible in its responses:
> In the way that we know that a car pulling up behind a stopped garbage truck is probably going to change lanes to get around it, having been built with 700,000 miles of driving data has helped the Google algorithm to understand that the car is likely to do such a thing [[18](http://www.alphr.com/cars/7038/how-do-googles-self-driving-cars-work)].

The self-driving car's machine learning algorithms allow it to predict possible behaviours and trajectories, compose a set of appropriate responses, and choose the response with the best likely outcome. 

## Obstacles to Widespread Adoption of Autonomous Vehicles

Whether we are ever to gain the aforementioned vehicular utopia imagined by Google remains to be seen -- the social, financial, political, and technological barriers to the proliferation of autonomous vehicles are not insignificant.

The social obstacles to the adoption of autonomous vehicles include imperfect understanding (due, perhaps, to misinformation or incomplete information), lack of trust, and lack of desire -- many people enjoy driving, and would rather not give it up. This headline from [CarThrottle](https://www.carthrottle.com/) sums up the lack of desire, if not outright hostility, well: 
> ### Google's Driverless Cars Could Spell Disaster for True Petrolheads 
> #### Google has developed this prototype driverless car from scratch; better make the most of driving while you can, then... [[27](https://www.carthrottle.com/post/googles-driverless-cars-could-spell-disaster-for-true-petrolheads/)]

The question of how one could manage to fully trust safety to an autonomous vehicle comes up in wondering how the vehicle would be programmed to act in the event of an unavoidable accident. An article in the MIT Technology Review posits that self driving cars "... can never be perfectly safe... [a]nd that raises some difficult issues". Most notably, how the vehicle should behave when an accident is imminent. The article queries: "Should [the self-driving car] minimize the loss of life, even if it means sacrificing the occupants, or should it protect the occupants at all costs? Should it choose between these extremes at random?". 

The MIT Review goes on to describe a study undertaken by Jean-Francois Bonnefon, at the Toulouse School of Economics, where "... several hundred workers on Amazon’s Mechanical Turk..." were asked for their opinions on the abovementioned questions. "The results are interesting, if predictable. In general, people are comfortable with the idea that self-driving vehicles should be programmed to minimize the death toll". However, "they actually wished others to cruise in utilitarian autonomous vehicles, more than they wanted to buy utilitarian autonomous vehicles themselves", expressing a lack of confidence that the vehicles would actually be programmed in this manner [[28](https://www.technologyreview.com/s/542626/why-self-driving-cars-must-be-programmed-to-kill/)].

Add to this the staggering rate at which cyber attacks occur [[29](http://www.hackmageddon.com/2016/01/11/2015-cyber-attacks-statistics/)], and it is all too easy to imagine that, whether through malevolence on the part of a company or black hat hacker, vehicles could be made to execute:
```javascript
// hostile machine takeover phase II
var killPedestrians = function() {
	locatePedestrians(idealTargets).then(function(locations) {
		hitPedestrians(locations);
	});
}
if (idealTargets.length > 0) {
	killPedestrians();
}
```
Not a single member of Google's self-driving test fleet has yet, however, to find itself in an imminent-accident predicament as outlined above. In fact, the fleet's accident record is nigh on impeccable. According to the _Google Self-Driving Car Project Monthly Report: November 2015_, in the six years the project has been operating, its vehicles have "been involved in 17 minor accidents during more than 2 million miles of autonomous and manual driving combined. Not once was the self-driving car the cause of the accident" [[34](https://static.googleusercontent.com/media/www.google.com/en//selfdrivingcar/files/reports/report-1115.pdf)]. On February 14, 2016, the first accident occurred where the autonomous vehicle could be said to have been at fault -- the vehicle collided with a bus as it was trying to enter into the lane in which the bus was travelling. According to the monthly report: 

>The Google AV test driver saw the bus approaching in the left side mirror but believed the bus would stop or slow to allow the Google AV to continue. Approximately three seconds later, as the Google AV was reentering the center of the lane it made contact with the side of the bus. The Google AV was operating in autonomous mode and traveling at less than 2 mph, and the bus was travelling at about 15 mph at the time of contact [[35](https://static.googleusercontent.com/media/www.google.com/en//selfdrivingcar/files/reports/report-0216.pdf)].

Even if the question of safety were to be completely, positively answered, the question of cost would remain. In its article, _Will You Ever Be Able to Afford A Self-Driving Car?_, [FastCompany](http://www.fastcompany.com/) estimates the current cost of Google's self-driving prototype vehicle to be $320,000 -- if this were to be the price of a vehicle once they are in production, owning a self-driving car would be the privilege of an elite few [[30](http://www.fastcompany.com/3025722/will-you-ever-be-able-to-afford-a-self-driving-car)]. Prototypes are, however, notoriously expensive due to their rarity, and the cost of the vehicle's most expensive components is decreasing demonstrably, and rapidly. As [WIRED](http://www.wired.com/) points out in its article, _Turns Out the Hardware in Self-Driving Cars is Pretty Cheap_, the prototype vehicle's LIDAR currently costs "... north of $50,000", and "[t]he next-gen Google car will have an $8,000 version" [[31](http://www.wired.com/2015/04/cost-of-sensors-autonomous-cars/)]. Once the self-driving vehicle is in production, it is likely that its price will one accessible to consumers with average incomes.

The political obstacles to legalizing vehicles not operated by humans are many. Driverless cars were decidedly a work of fiction when the laws concerning driving were written, and thus, presume a constant human presence. They need to be modified in order to allow for vehicles that operate in the absence of a human pilot. Luckily, for Google, this is already happening. According to WIRED, the United States of America's "... federal government says that when it comes to regulating self-driving cars, computers and software systems can be considered the 'driver' of the vehicle" [[32](http://www.wired.com/2016/02/feds-say-theyll-count-computers-as-human-drivers/)]. This is just one country, and only at the federal level -- laws throughout all countries in which autonomous vehicles would operate, and throughout all of the levels at which laws concerning vehicles exist, would need to be appropriately modified, but it is a beginning.

The technological obstacles that still need to be worked out before self-driving cars could even think of being ubiquitous involve dealing with inclement conditions, accurate sensing of traffic lights when the sensors are blinded, and the gargantuan task of acquiring the detailed maps required for the vehicles of all the roads, everywhere. In _How Self-Driving Cars Work: The Nuts and Bolts Behind Google's Autonomous Car Program_, Bryan Clark points out: "So far, the car has issues that would prevent it from driving in snow, ice or heavy rain", and the vehicle is "unable to tell the color of traffic lights when sensors are blinded by sun or glare" [[33](http://www.makeuseof.com/tag/how-self-driving-cars-work-the-nuts-and-bolts-behind-googles-autonomous-car-program/)]. Also, as has been explained previously, the maps required by the car before it is able to operate in a given environment need to be of excruciating detail. "So far, Google has mapped 2,000 miles of road. The US road network [alone] has something like 4 million miles of road" [[17](http://www.theatlantic.com/technology/archive/2014/05/all-the-world-a-track-the-trick-that-makes-googles-self-driving-cars-work/370871/)]. It would appear that the erosion of this barrier, like the financial one, could be just a matter of time, given sufficient work towards the goal.

## Conclusion

Given that a number of advanced automated processes are already available in vehicles in production today (albeit in luxury vehicles, such as Volvo's S90 and Tesla's Model S), it is clear that the experience of driving has changed dramatically since the emergence of the modern car. Google is poised to make that change even more dramatic, should it find its way around the social, political, financial, and technological barriers currently in place. It is unlikely that all drivers will welcome this change with open arms, but many drivers, as well as those who are unable to drive, could stand to benefit greatly from access to a vehicle that pilots itself.

## References

1. [Where To? A History of Autonomous Vehicles](http://www.computerhistory.org/atchm/where-to-a-history-of-autonomous-vehicles/), Marc Weber, Computer History Museum.
2. [Self-steering gear](https://en.wikipedia.org/wiki/Self-steering_gear), Wikipedia
3. [Gyroscopic Autopilot](https://en.wikipedia.org/wiki/Gyroscopic_autopilot), Wikipedia
4. [The Living Machine, Wonder Stories, v06 12](http://comicbookplus.com/?dlid=36256), David H. Keller
5. ['Phantom Auto' Will Tour City](https://news.google.com/newspapers?id=unBQAAAAIBAJ&sjid=QQ8EAAAAIBAJ&pg=7304%2C3766749), The Milwawaukee Sentinel, 1926-12-08
6. [GM Concept 1956 Firebird II](http://www.oldcarbrochures.com/static/NA/GM%20Corporate%20and%20Concepts/1956_GM_Firebird_II/1956%20Firebird%20II-12-13.html)
7. [Automated](http://www.merriam-websmter.com/dictionary/automated), Merriam Webster
8. [Autonomy](http://www.merriam-webster.com/dictionary/autonomy), Merriam Webster
9. [Tesla: Your Autopilot has arrived](https://www.teslamotors.com/en_CA/blog/your-autopilot-has-arrived)
10. [Audi's self-driving car: Hands off the steering wheel!](http://fortune.com/2015/01/28/audis-self-driving-car/)
11. Volvo: [The S90](http://www.volvocars.com/en-ca/cars/new-models/s90)
12. [Google's Self-Driving Car Project: How](https://www.google.com/selfdrivingcar/how/)
13. [Google's Self-Driving Car Project: Where](https://www.google.com/selfdrivingcar/where/)
14. [Google's Self-Driving Car Project](https://www.google.com/selfdrivingcar/)
15. [LIDAR](http://oceanservice.noaa.gov/facts/lidar.html)
16. [The Trick That Makes Google's Self-driving Cars Work](http://www.theatlantic.com/technology/archive/2014/05/all-the-world-a-track-the-trick-that-makes-googles-self-driving-cars-work/370871/)
17. [Ford: Ford Doubles Down on Autonomous Vehicles](http://blog.ford.ca/2016/01/06/ford-doubles-down-on-autonomous-vehicles/)
18. Alphr, [How do Google's self-driving cars work?](http://www.alphr.com/cars/7038/how-do-googles-self-driving-cars-work)
19. [Radar](https://www.iop.org/publications/iop/2011/file_47456.pdf), Institute of Physics
20. [How does GPS work?](http://www.physics.org/article-questions.asp?id=55)
21. [What is trilateration?](http://www.mio.com/technology-trilateration.htm), Mio
22. [10 astonishing technologies that power Google's self-driving cars](http://www.national.co.uk/tech-powers-google-car/)
23. [Ultrasonic Sensing](http://www.ab.com/en/epub/catalogs/12772/6543185/12041221/12041229/print.html), Rockwell Automation
24. [Altimeter](http://education.nationalgeographic.org/encyclopedia/altimeter/)
25. [Gyroscope](http://www.oxforddictionaries.com/definition/english/gyroscope), Oxford Dictionaries
26. [Tachometer](http://www.encyclopedia.com/topic/tachometer.aspx)
27. [Driverless Cars Could Spell Disaster for True Petrolheads](https://www.carthrottle.com/post/googles-driverless-cars-could-spell-disaster-for-true-petrolheads/)
28. [Why Self-driving Cars Must Be Programmed to Kill](https://www.technologyreview.com/s/542626/why-self-driving-cars-must-be-programmed-to-kill/)
29. [2015 Cyber Attacks Statistics](http://www.hackmageddon.com/2016/01/11/2015-cyber-attacks-statistics/)
30. [Will You Ever Be Able To Afford A Self-Driving Car?](http://www.fastcompany.com/3025722/will-you-ever-be-able-to-afford-a-self-driving-car)
31. [Turns Out the Hardware in Self-Driving Cars is Pretty Cheap](http://www.wired.com/2015/04/cost-of-sensors-autonomous-cars/)
32. [Feds Say They'll Count Computers as Human Drivers](http://www.wired.com/2016/02/feds-say-theyll-count-computers-as-human-drivers/)
33. [How Self-Driving Cars Work: The Nuts and Bolts Behind Google's Autonomous Car Program](http://www.makeuseof.com/tag/how-self-driving-cars-work-the-nuts-and-bolts-behind-googles-autonomous-car-program/)
34. [Google Self-Driving Car Project Monthly Report, November 2015](https://static.googleusercontent.com/media/www.google.com/en//selfdrivingcar/files/reports/report-1115.pdf)
35. [Google Self-Driving Car Project Monthly Report, February 2016](https://static.googleusercontent.com/media/www.google.com/en//selfdrivingcar/files/reports/report-0216.pdf)

