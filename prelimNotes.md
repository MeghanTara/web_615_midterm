# Autonomous Vehicles

The Past, Present & Future of
autonomous vehicles

## Preliminary Notes / Bibliography

## Focus areas:
* What will be the direct and indirect effects on society and business with an increasing number of autonomous vehicles on the road?
* What are the current and future obstacles for autonomous vehicle technology?
* Who are the companies currently involved in autonomous vehicles? What are their goals and motivations?
* How will autonomous vehicles intersect and interact with the internet and web technologies?

## Research:
### Wikipedia 
- [Autonomous car](https://en.wikipedia.org/wiki/Autonomous_car)
- [Tesla's level 3 autonomous Model S](https://en.wikipedia.org/wiki/Tesla_Model_S#Autopilot)
- [Mobility as a service](https://en.wikipedia.org/wiki/Mobility_as_a_service_(transport))
- [Google self-driving car](https://en.wikipedia.org/wiki/Google_self-driving_car)

### History of:
- [Self-steering Gear](https://en.wikipedia.org/wiki/Self-steering_gear), for sailboats
- [Gyroscopic autopilot](https://en.wikipedia.org/wiki/Gyroscopic_autopilot)
- [How Autopilot Works](http://science.howstuffworks.com/transport/flight/modern/autopilot.htm)
- [Where to? A History of Autonomous Vehicles](http://www.computerhistory.org/atchm/where-to-a-history-of-autonomous-vehicles/)
- [Autonomous Cars Through the Ages](http://www.wired.com/2012/02/autonomous-vehicle-history/)
- In science fiction, [The Living Machine, Wonder Stories, v06 12](http://comicbookplus.com/?dlid=36256), David H. Keller
- Early concept vehicle, ['Phantom Auto' Will Tour City](https://news.google.com/newspapers?id=unBQAAAAIBAJ&sjid=QQ8EAAAAIBAJ&pg=7304%2C3766749), The Milwawaukee Sentinel, 1926-12-08
- [GM Concept 1956 Firebird II](http://www.oldcarbrochures.com/static/NA/GM%20Corporate%20and%20Concepts/1956_GM_Firebird_II/1956%20Firebird%20II-12-13.html)

### Technology
- Google
	- [LIDAR](http://oceanservice.noaa.gov/facts/lidar.html)
	- Make Use Of, [How Self-Driving Cars Work: The Nuts and Bolts Behind Google's Autonomous Car Program](http://www.makeuseof.com/tag/how-self-driving-cars-work-the-nuts-and-bolts-behind-googles-autonomous-car-program/)
	- Alphr, [How do Google's self-driving cars work?](http://www.alphr.com/cars/7038/how-do-googles-self-driving-cars-work)
	- [10 astonishing technologies that power Google's self-driving cars](http://www.national.co.uk/tech-powers-google-car/)


### Press releases
- ['Mobility as a sevice' - the new transport model?](http://www.itsineurope.com/its10/media/press_clippings/ITS%20Supp_et214.pdf)
- [Volvo Car Corporation aims for leadership within autonomous driving technology](https://www.media.volvocars.com/ca/en-ca/media/pressreleases/45718) 
- [Volvo Debuts Autonomous Concept Car](https://www.media.volvocars.com/ca/en-ca/media/pressreleases/169691/volvo-cars-debuts-autonomous-drive-concept-at-los-angeles-auto-show)


### Company website
- [Google's Self-Driving Car Project](https://www.google.com/selfdrivingcar/)
- [Google's Self-Driving Car Project: How](https://www.google.com/selfdrivingcar/how/)
- [Google's Self-Driving Car Project: Where](https://www.google.com/selfdrivingcar/where/)
- [Google's Self-Driving Car Project Monthly Reports](https://www.google.com/selfdrivingcar/reports/)
- [Google Self-Driving Car Project Monthly Report, September 2015](https://static.googleusercontent.com/media/www.google.com/en//selfdrivingcar/files/reports/report-0915.pdf)
- [Google Self-Driving Car Project Monthly Report, November 2015](https://static.googleusercontent.com/media/www.google.com/en//selfdrivingcar/files/reports/report-1115.pdf)
- [Google Self-Driving Car Project Monthly Report, February 2016](https://static.googleusercontent.com/media/www.google.com/en//selfdrivingcar/files/reports/report-0216.pdf)
- [Volvo's Autopilot](http://www.volvocars.com/intl/about/our-innovation-brands/intellisafe/intellisafe-autopilot)
    - automated processes already in some vehicles: [The S90](http://www.volvocars.com/en-ca/cars/new-models/s90)
- Mio, [What is trilateration?](http://www.mio.com/technology-trilateration.htm)

### Company blog
- [Tesla: Your Autopilot has arrived](https://www.teslamotors.com/en_CA/blog/your-autopilot-has-arrived)
- [Tesla: Summon Your Tesla from Your Phone](https://www.teslamotors.com/en_CA/blog/summon-your-tesla-your-phone)
- [Ford: Ford Doubles Down on Autonomous Vehicles](http://blog.ford.ca/2016/01/06/ford-doubles-down-on-autonomous-vehicles/)
- [Audi piloted driving](http://www.audi.ca/ca/web/en/vorsprung-durch-technik/content/2014/10/piloted-driving.html)

### Governing bodies
- [UK Gov't: Driverless car tech funding](https://www.gov.uk/government/news/driverless-cars-technology-receives-20-million-boost)
- [DMV Releases Draft Requirements for Public Deployment of Autonomous Vehicles, State Seeks Public Comment on Draft Document](https://www.dmv.ca.gov/portal/dmv/detail/pubs/newsrel/newsrel15/2015_63), California Department of Motor Vehicles
- Press Release, [Ontario First to Test Automated Vehicles on Roads in Canada](https://news.ontario.ca/mto/en/2015/10/ontario-first-to-test-automated-vehicles-on-roads-in-canada.html)
- Ontario, [A Pilot Project to Safely Test Autonomous Vehicles - Summary of Proposal](http://www.ontariocanada.com/registry/showAttachment.do?postingId=14802&attachmentId=22912)

### Well respected fan sites
- Car Throttle, [Driverless Cars Could Spell Disaster for True Petrolheads](https://www.carthrottle.com/post/googles-driverless-cars-could-spell-disaster-for-true-petrolheads/)
- [2015 Cyber Attacks Statistics](http://www.hackmageddon.com/2016/01/11/2015-cyber-attacks-statistics/)

### Third party vendors/manufacturers
- [A Scenario: The End of Auto Insurance](http://www.celent.com/reports/scenario-end-auto-insurance)
- [How Google's Self-Driving Car Works](http://spectrum.ieee.org/automaton/robotics/artificial-intelligence/how-google-self-driving-car-works)
- [The Roadmap for Autonomous (Self-Driving) Vehicles in Ontario, Canada](http://www.ogra.org/files/Roadmap%20for%20AVs%20in%20Ontario%20-%20White%20Paper.pdf)
- [Ultrasonic Sensing](http://www.ab.com/en/epub/catalogs/12772/6543185/12041221/12041229/print.html), Rockwell Automation
- [Altimeter](http://education.nationalgeographic.org/encyclopedia/altimeter/)

### Public records

### University/gov't sources
- [46 New Eng. L. Rev. 581 (2011-2012) Look Ma, No Hands: Wrinkles and Wrecks in the Age of Autonomous Vehicles](http://heinonline.org/HOL/LandingPage?handle=hein.journals/newlr46&div=30&id=&page=)
- [Vehicle Tracking using Ultrasonic Sensors & Joined Particle Weighting](http://www.elektromobilitaet.fraunhofer.de/content/dam/elektromobilitaet/de/documents/fsem_ii/pf_final_sub_pk.pdf)

### News
- Reuters
	- [Google expands self-driving car testing to Washington State](http://www.reuters.com/article/us-alphabet-autos-testing-idUSKCN0VC26R)
	- [Automakers, not Silicon Valley, lead in driverless car patents: study](http://www.reuters.com/article/us-tech-ces-autos-idUSKBN0UJ1UD20160105)
	- [Obama administration to announce efforts to boost self-driving cars](http://www.reuters.com/article/us-usa-obama-autos-idUSKCN0UQ2F620160112)
- Wired
    - [Feds Say They'll Count Computers as Human Drivers](http://www.wired.com/2016/02/feds-say-theyll-count-computers-as-human-drivers/)
    - [The Case for Making Self-driving Cars Think Like Humans](http://www.wired.com/2016/02/the-case-for-making-self-driving-cars-think-like-humans/)
    - [Toyota Eyes Satellite Antennas for Data-hungry Connected Cars](http://www.wired.com/2016/01/toyota-eyes-satellite-antennas-for-data-hungry-connected-cars/)
    - [Obesessing Over AI Is the Wrong Way to Think About the Future](http://www.wired.com/2016/01/forget-ai-the-human-friendly-future-of-computing-is-already-here/)
    - [Turns Out the Hardware in Self-Driving Cars is Pretty Cheap](http://www.wired.com/2015/04/cost-of-sensors-autonomous-cars/)
- cnet
    - [Self-driving Volkswagen completes 1500 mile Mexican trek](http://www.cnet.com/roadshow/news/self-driving-volkswagen-completes-1500-mile-mexican-trek/)
- MIT Technology Review
    - [Why Self-driving Cars Must Be Programmed to Kill](https://www.technologyreview.com/s/542626/why-self-driving-cars-must-be-programmed-to-kill/)
- The Atlantic
	- [The Trick That Makes Google's Self-driving Cars Work](http://www.theatlantic.com/technology/archive/2014/05/all-the-world-a-track-the-trick-that-makes-googles-self-driving-cars-work/370871/)
- The Washington Post
	- [These charts show who's lapping whom in the race to the perfect driverless car](https://www.washingtonpost.com/news/the-switch/wp/2016/01/15/how-googles-driverless-car-stacks-up-against-the-competition/)
- ZDNet
	- [Google's autonomous car injuries: Blame the human](http://www.zdnet.com/article/googles-autonomous-car-injuries-blame-the-human/)
- CBC
	- [Ontario's driverless car project 'really exciting' for industry](http://www.cbc.ca/news/technology/driverless-cars-ontario-pilot-project-1.3268641)
- The Globe and Mail
	- [Ontario moves to permit testing of self-driving vehicles](http://www.theglobeandmail.com/report-on-business/ontario-moves-to-permit-testing-of-self-driving-vehicles/article26778157/)
	- [Toyota shows self-driving car, promises to start selling by 2020](http://www.theglobeandmail.com/globe-drive/culture/technology/toyota-shows-self-driving-car-promises-to-start-selling-by-2020/article26683828/)
- The Guardian
	- [Google reports self-driving car mistakes: 272 failures and 13 near misses](http://www.theguardian.com/technology/2016/jan/12/google-self-driving-cars-mistakes-data-reports)
- BBC
	- [Google self-driving car hits a bus](http://www.bbc.com/news/technology-35692845)
- The University Record (U of M), [Ford, U-M testing autonomous cars in snow at Mcity](http://record.umich.edu/articles/ford-u-m-testing-autonomous-cars-snow-mcity)
- Fortune
	- [Audi's self-driving car: Hands off the steering wheel!](http://fortune.com/2015/01/28/audis-self-driving-car/)
- FastCompany
	- [Will You Ever Be Able To Afford A Self-Driving Car?](http://www.fastcompany.com/3025722/will-you-ever-be-able-to-afford-a-self-driving-car)

### NGO's
- [Autonomous Vehicle Technology: A Guide for Policymakers](http://www.rand.org/content/dam/rand/pubs/research_reports/RR400/RR443-1/RAND_RR443-1.pdf)
- [Radar](https://www.iop.org/publications/iop/2011/file_47456.pdf), Institute of Physics
- [How does GPS work?](http://www.physics.org/article-questions.asp?id=55)


### Definitions
- [Autonomy](http://www.merriam-webster.com/dictionary/autonomy), Merriam Webster
- [Automated](http://www.merriam-webster.com/dictionary/automated), Merriam Webster
- [Gyroscope](http://www.oxforddictionaries.com/definition/english/gyroscope), Oxford Dictionaries
- [Tachometer](http://www.encyclopedia.com/topic/tachometer.aspx)


